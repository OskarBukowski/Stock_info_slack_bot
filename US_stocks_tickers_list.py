import requests
from bs4 import BeautifulSoup as bs
import pandas as pd
from selenium import webdriver



def main():

    url = 'https://stockanalysis.com/stocks/'

    driver = webdriver.Chrome('Chromedriver/chromedriver.exe')
    driver.minimize_window()
    driver.get(url)
    element = driver.find_element_by_xpath('//*[@id="main"]/div/div/nav/div/select/option[3]')
    element.click()

    table = pd.read_html(driver.find_element_by_xpath('//*[@id="symbol-table"]').get_attribute('outerHTML'))[0]

    print(table)
    return table




if __name__ == '__main__':
    main()