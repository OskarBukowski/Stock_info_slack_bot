import slack_sdk
import os
from pathlib import Path
from dotenv import load_dotenv  # to load .env file
import time
from slackeventsapi import SlackEventAdapter
from flask import Flask

import stock_data_output


#### Loading the environment variable ###

env_path = Path('.') / '.env'
load_dotenv(dotenv_path=env_path)

app = Flask(__name__)
slack_event_adapter = SlackEventAdapter(os.environ['SIGNING_SECRET'], '/slack/events', app)

client = slack_sdk.WebClient(token=os.environ['SLACK_TOKEN'])


### channel has to have the app added to its settings, unless it won't work


# client.chat_postMessage(channel='test', text = 'example text')

@slack_event_adapter.on('input')
def message(input):
    input = input.get('event', {})['text']
    id = input.get('event', {})['user']

    if id != client.api_call('auth.test')['user_id']:
        client.chat_postMessage(channel='test', text='xxx')


if __name__ == '__main__':
    app.run(debug=True)

# to run ngrok : ngrok http <port>
