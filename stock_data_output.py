import pandas
import numpy as np
import yfinance as yf
import re

input_data = 'AAPL.price'


class Commands_navigator():

    def __init__(self):
        pass

    @staticmethod
    def commands(string_command):
        ticker, function = string_command.split('.')

        global commands_dict

        commands_dict = {
            'help': [Commands_navigator.commands(input), 'prints the whole possible features'],
            'price': [Stock_info.current_price(input), 'prints the current stock price'],
            'volume': [Stock_info.current_volume(input), 'prints the current stock volume'],
            'price_<period>_<option>': [Stock_info.price_parameters(input), 'prints the specified option within given period of time ; periods = [m, h, d, m, y] , options = [ max, min, mean, change ]'],
        }

        return commands_dict



class Stock_info():
    def __init__(self, input_command):
        self.ticker = input_command


    def current_price(self):
        return yf.download(tickers=self.ticker, period='5m', interval='1m')['Close'].iloc[1]

    # def volume(self):
    #     return yf.download(tickers=self.ticker, period='5m', interval='1m')['Volume'].iloc[1]

    # @classmethod
    # def period_price_change(self, period, option):
    #     pass




